package com.example.secrettinder

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.example.secrettinder.algo.Alicja
import com.example.secrettinder.algo.Bob
import com.example.secrettinder.data.User
import com.example.secrettinder.utils.Common
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

private lateinit var db: DatabaseReference

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        db = Firebase.database.reference

        db.child("users").get().addOnSuccessListener { that ->
            Log.i("firebase", "Got value users list: ${that.value}")
            val usersMap: Map<String, Map<String, String>> =
                that.value as Map<String, Map<String, String>>
            val users: List<User> = usersMap.map { User(it.value) }
            showUsers(users)
        }.addOnFailureListener {
            Log.e("firebase", "Error getting data", it)
        }
    }

    private fun showUsers(users: List<User>) {
        val iterator = users.iterator()
        val u: User = iterator.next()
        showUser(u, iterator)
    }

    private fun showUser(user: User, iterator: Iterator<User>) {
        if (user.name == Common.user.name) {
            val u: User = iterator.next()
            showUser(u, iterator)
            return
        }
        findViewById<TextView>(R.id.friend_name_info).text = user.name
        findViewById<TextView>(R.id.friend_description_info).text = user.description
        findViewById<Button>(R.id.no_button).setOnClickListener {
            createQuery(user.name, false)
            val u: User = iterator.next()
            showUser(u, iterator)
        }
        findViewById<Button>(R.id.yes_button).setOnClickListener {
            createQuery(user.name, true)
            val u: User = iterator.next()
            showUser(u, iterator)
        }

    }

    private fun createQuery(friendName: String, b: Boolean) {
        val queryId = generateQueryId(friendName)
        db.child("existedQueries").get().addOnSuccessListener { that ->
            Log.i("firebase", "Got value users list: ${that.value}")
            if (that.value == null) {
                Alicja(queryId, friendName, b, db, this)
            } else {
                val existedQueries: Map<String, Boolean> = that.value as Map<String, Boolean>
                if (queryId !in existedQueries.keys || existedQueries[queryId] == false) {
                    Alicja(queryId, friendName, b, db, this)
                } else {
                    Bob(queryId, friendName, b, db, this)
                }
            }

        }.addOnFailureListener {
            Log.e("firebase", "Error getting data", it)
        }
    }

    private fun generateQueryId(friendName: String): String {
        return if (friendName > Common.user.name) {
            friendName + Common.user.name
        } else {
            Common.user.name + friendName
        }
    }
}
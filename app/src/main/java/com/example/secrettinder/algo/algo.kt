package com.example.secrettinder.algo

import android.content.Context
import android.content.Intent
import android.util.Log
import com.example.secrettinder.MainActivity
import com.example.secrettinder.MatchActivity
import com.example.secrettinder.data.*
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import java.math.BigInteger
import java.util.*
import javax.crypto.AEADBadTagException
import javax.crypto.Cipher
import javax.crypto.spec.GCMParameterSpec
import javax.crypto.spec.SecretKeySpec

class AES {
    companion object {
        fun encrypt(keyBI: BigInteger, plaintextBI: BigInteger): BigInteger {
            val key = keyBI.toByteArray().take(16).toByteArray()
//            Log.d("myDebug", "key = ${key.toList()}")
            val plaintext = plaintextBI.toByteArray()
//            Log.d("myDebug", "plain = ${plaintext.toList()}")
            val secretKeySpec = SecretKeySpec(key, "AES")
            val ivParameterSpec = GCMParameterSpec(128, key)
            val cipher = Cipher.getInstance("AES/GCM/NoPadding")
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec)
            val res = cipher.doFinal(plaintext)
//            Log.d("myDebug", "res = ${res.toList()}")
            return BigInteger(res)
        }

        fun decrypt(keyBI: BigInteger, messageBI: BigInteger): BigInteger {
            val key = keyBI.toByteArray().take(16).toByteArray()
//            Log.d("myDebug", "key = ${key.toList()}")
            val message = messageBI.toByteArray()
//            Log.d("myDebug", "m = ${message.toList()}")
            val secretKeySpec = SecretKeySpec(key, "AES")
            val ivParameterSpec = GCMParameterSpec(128, key)
            val cipher = Cipher.getInstance("AES/GCM/NoPadding")
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec)
            val res = cipher.doFinal(message)
//            Log.d("myDebug", "res = ${res.toList()}")
            return BigInteger(res)
        }
    }

}

class Alicja(val queryId: String, val friendName: String, b: Boolean, val db: DatabaseReference, val context : Context) {
    var res1: Pair<QueryStage1, List<BigInteger>>
    var queryStage1Producer: QueryStage1Producer
    var stage = "stage2"
    var queryStage3Producer: QueryStage3Producer? = null

    init {
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.value != null) {
                    val post = dataSnapshot.value as Map<String, Any>
                    if (stage in post.keys) {
                        when (stage) {
                            "stage2" -> sendStage3(post[stage] as Map<String, String>)
                            "stage4" -> sendStage5(post[stage] as Map<String, String>)
                        }
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("myDebug", "loadPost:onCancelled", databaseError.toException())
            }
        }
        val postReference = db.child("queries").child(queryId)
        postReference.addValueEventListener(postListener)
        Log.d("myDebug", "Jestem Alicją")
        db.child("existedQueries").child(queryId).setValue(true).addOnSuccessListener {
            Log.i("firebase", "add existedQueries")
        }.addOnFailureListener {
            Log.e("firebase", "Error getting data", it)
        }
        res1 = algo(b)
        queryStage1Producer = QueryStage1Producer(res1.first)
        db.child("queries").child(queryId).child("stage1").setValue(res1.first)
            .addOnSuccessListener {
                Log.i("firebase", "add Query Stage1")
            }.addOnFailureListener {
                Log.e("firebase", "Error getting data", it)
            }
    }

    private fun sendStage3(stage2: Map<String, String>) {
        val queryStage2 = QueryStage2(stage2["v"]!!)
        val queryStage2Producer = QueryStage2Producer(queryStage2)
        val res = obviousTransfer(queryStage1Producer.xBI, res1.second, queryStage2Producer.vBI)
        queryStage3Producer = QueryStage3Producer(res)
        db.child("queries").child(queryId).child("stage3")
            .setValue(queryStage3Producer!!.getQueryStage3()).addOnSuccessListener {
                Log.i("firebase", "add Query Stage3")
            }.addOnFailureListener {
                Log.e("firebase", "Error getting data", it)
            }
        stage = "stage4"
    }
    private fun sendStage5(stage4: Map<String, String>) {
        val queryStage4 = QueryStage4(stage4["result"]!!)
        val queryStage4Producer = QueryStage4Producer(queryStage4)
        val resultBool = queryStage1Producer.outBI.indexOf(queryStage4Producer.resultBI) == 1
        val queryStage5 = QueryStage5(resultBool = resultBool)
        db.child("queries").child(queryId).child("stage5")
            .setValue(queryStage5).addOnSuccessListener {
                Log.i("firebase", "add Query Stage5")
            }.addOnFailureListener {
                Log.e("firebase", "Error getting data", it)
            }


        if(resultBool) {
            val intent = Intent(context, MatchActivity::class.java)
            intent.putExtra("friend", friendName)
            context.startActivity(intent)
        }
    }

}

class Bob(val queryId: String, val friendName: String, val b: Boolean, val db: DatabaseReference, val context : Context) {
    var stage = "stage3"
    var queryStage1Producer: QueryStage1Producer? = null
    var res1: Pair<BigInteger, BigInteger>? = null

    init {
        Log.d("myDebug", "Jestem Bobem")
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if(dataSnapshot.value != null) {
                    val post = dataSnapshot.value as Map<String, Any>
                    if (stage in post.keys) {
                        when (stage) {
                            "stage3" -> sendStage4(post[stage] as Map<String, List<String>>)
                            "stage5" -> printResult(post[stage] as Map<String, Boolean>)
                        }
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("myDebug", "loadPost:onCancelled", databaseError.toException())
            }
        }
        val postReference = db.child("queries").child(queryId)
        postReference.addValueEventListener(postListener)
        postReference.child("stage1").get().addOnSuccessListener { that ->
            Log.i("firebase", "Got value users list: ${that.value}")
            val res: Map<String, Any> = that.value as Map<String, List<Int>>
            queryStage1Producer = QueryStage1Producer(
                QueryStage1(
                    res["ka"] as String,
                    res["x"] as List<String>,
                    res["out"] as List<String>,
                    res["c"] as List<String>
                )
            )
            res1 = bobGenVK(b, queryStage1Producer!!.xBI)
            val queryStage2Producer = QueryStage2Producer(res1!!.first)
            db.child("queries").child(queryId).child("stage2")
                .setValue(queryStage2Producer.getQueryStage2()).addOnSuccessListener {
                    Log.i("firebase", "add Query Stage2")
                }.addOnFailureListener {
                    Log.e("firebase", "Error getting data", it)
                }

        }.addOnFailureListener {
            Log.e("firebase", "Error getting data", it)
        }
    }

    private fun printResult(map: Map<String, Boolean>) {
        if(map["resultBool"]!!) {
            val intent = Intent(context, MatchActivity::class.java)
            intent.putExtra("friend", friendName)
            context.startActivity(intent)
        }
    }

    private fun sendStage4(stage3: Map<String, List<String>>) {
        val queryStage3 = QueryStage3(stage3["mp"]!!)
        val queryStage3Producer = QueryStage3Producer(queryStage3)
        val mb = bobDecryptM(queryStage3Producer.mpBI, res1!!.second, b)
        val mOut = bob(queryStage1Producer!!, mb)
        val queryStage4Producer = QueryStage4Producer(mOut)
        db.child("queries").child(queryId).child("stage4")
            .setValue(queryStage4Producer.getQueryStage4()).addOnSuccessListener {
                Log.i("firebase", "add Query Stage4")
            }.addOnFailureListener {
                Log.e("firebase", "Error getting data", it)
            }
        stage = "stage5"
    }
}

fun algo(kaBin: Boolean): Pair<QueryStage1, List<BigInteger>> {
//    aesTest()

    val m = 16
    val ka0 = BigInteger(m * 8, Random())//BigInteger("99544536007543913583929620215415265483")//m * 8, Random())
    val ka1 = BigInteger(m * 8, Random())//BigInteger("245961601083971558180221924854495002802")//m * 8, Random())
    val kb = List(2) { BigInteger(m * 8, Random()) }//listOf(BigInteger("282243323254805442707247306788502401321"), BigInteger("97121189840109609621131834987684917574"))//List(2) { BigInteger(m * 8, Random()) }
    val out = List(2) { BigInteger(m * 8, Random()) }//listOf(BigInteger("172555194971460357420794875704262802820"), BigInteger("161076344502973803035854993402513331953"))//List(2) { BigInteger(m * 8, Random()) }
    val c: MutableList<BigInteger> = mutableListOf()

    for (a in 0..1) {
        for (b in 0..1) {
            val kka = listOf(ka0, ka1)[a]
            val kkb = kb[b]
            val kout = if (a + b < 2) {
                out[0]
            } else {
                out[1]
            }
            val c1 = AES.encrypt(kkb, kout)
            val c2 = AES.encrypt(kka, c1)
            c.add(c2)
        }
    }

    val ka = if (kaBin) {
        ka1
    } else {
        ka0
    }

    val x = List(2) { BigInteger(m * 8, Random())}
    val queryProducer = QueryStage1Producer(ka, x, out, c)
    return Pair(queryProducer.getQueryStage1(), kb)
}

fun aesTest() {
    val k = BigInteger("99544536007543913583929620215415265483")
    val m = BigInteger("172555194971460357420794875704262802820")
    val c = AES.encrypt(k, m)
    val m2 = AES.decrypt(k, c)
    println(m2)
}

fun bobGenVK(
    b: Boolean,
    x: List<BigInteger>,
    n: BigInteger = BigInteger("2112664634855999140031945945998785346946804826144846396410436155861557104011009549879696604291518474904522547"),
    e: Int = 65537
): Pair<BigInteger, BigInteger> {
    val k = BigInteger(128, Random())
    val idx = if (b) 1 else 0
    val v = (x[idx] + k.pow(e)).mod(n)
    return Pair(v, k)
}

fun obviousTransfer(
    x: List<BigInteger>,
    kb: List<BigInteger>,
    v: BigInteger,
    d: BigInteger = BigInteger("585377376205045827301220782663105468898592075285171211065018186416365699827074434761795565062334913589643145"),
    n: BigInteger = BigInteger("2112664634855999140031945945998785346946804826144846396410436155861557104011009549879696604291518474904522547"),
): List<BigInteger> {
    val m0 = kb[0]
    val m1 = kb[1]
    val x0 = x[0]
    val x1 = x[1]
    val k0 = (v - x0).modPow(d, n)
    val k1 = (v - x1).modPow(d, n)
    return arrayListOf(m0 + k0, m1 + k1)
}

fun bobDecryptM(
    m: List<BigInteger>,
    k: BigInteger,
    b: Boolean
): BigInteger {
    val idx = if (b) 1 else 0
    return m[idx] - k
}

fun bob(
    qb: QueryStage1Producer,
    mb: BigInteger
): BigInteger {
    val kb = mb
    for (ci in qb.cBI) {
        try {
            val e1 = AES.decrypt(qb.kaBI, ci)
            val dec = AES.decrypt(kb, e1)
            if (dec in qb.outBI) {
                Log.d("myDebug", "decrypt pass")
                return dec
            }
        }
        catch (e : AEADBadTagException){
            Log.d("myDebug", "decrypt fail")
            continue
        }
    }
    return BigInteger.ZERO
}


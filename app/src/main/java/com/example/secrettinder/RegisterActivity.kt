package com.example.secrettinder

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import com.example.secrettinder.data.User
import com.example.secrettinder.utils.Common
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase


private lateinit var db: DatabaseReference
class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.registry)
    }

    fun registerUser(view: View) {
        val name : String = findViewById<EditText>(R.id.editTextTextPersonName).text.toString()
        val contact: String = findViewById<EditText>(R.id.editTextTextPersonContact).text.toString()
        val desc: String = findViewById<EditText>(R.id.editTextTextPersonDescription).text.toString()

        val user = User(name, contact, desc)

        db = Firebase.database.reference

        db.child("users").child(name).setValue(user).addOnSuccessListener {
            Log.i("firebase", "Registered")
            Common.user = user
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }.addOnFailureListener{
            Log.e("firebase", "Error getting data", it)
        }

    }
}
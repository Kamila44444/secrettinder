package com.example.secrettinder

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.example.secrettinder.data.User
import com.example.secrettinder.utils.Common
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase


private lateinit var db: DatabaseReference

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.welcome_window)
        db = Firebase.database.reference

        db.child("users").get().addOnSuccessListener { that ->
            Log.i("firebase", "Got value users list: ${that.value}")
            val users = that.value
            findViewById<Button>(R.id.button_login).setOnClickListener {
                val username : String  = findViewById<EditText>(R.id.login).text.toString()
                loginTry(
                    users as Map<String, Map<String, String>>,
                    users.keys,
                    username
                )
            }
        }.addOnFailureListener {
            Log.e("firebase", "Error getting data", it)
        }
        findViewById<Button>(R.id.button_sign).setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

    }

    private fun loginTry(
        users: Map<String, Map<String, String>>,
        usersNames: Set<String>,
        username: String
    ) {
        if (username !in usersNames) {
            Log.e("login", "wrong username")
            return
        }
        val userMap = users[username]!!
        val user = User(username, userMap["contact"]!!, userMap["description"]!!)
        Common.user = user
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)

    }
}
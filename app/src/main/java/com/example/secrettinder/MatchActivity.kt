package com.example.secrettinder

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.secrettinder.data.User
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

private lateinit var db: DatabaseReference

class MatchActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.match)
//        val bundle :Bundle ?=intent.extras
//        val message = bundle!!.getString("value") // 1
        val friendName: String = intent.getStringExtra("friend")!!// 2

        db = Firebase.database.reference

        db.child("users").child(friendName).get().addOnSuccessListener { that ->
            Log.i("firebase", "Got value users list: ${that.value}")
            val userMap = that.value as Map<String, String>
            val user = User(userMap["name"]!!, userMap["contact"]!!, userMap["description"]!!)
            findViewById<TextView>(R.id.textView2).text =
                "z ${user.name}, contact : ${user.contact}"
        }.addOnFailureListener {
            Log.e("firebase", "Error getting data", it)
        }
    }
}
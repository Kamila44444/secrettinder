package com.example.secrettinder.data

import java.math.BigInteger

data class QueryStage1(
    var ka: String = "",
    var x: List<String> = emptyList(),
    var out: List<String> = emptyList(),
    var c: List<String> = emptyList<String>().toMutableList()
)

class QueryStage1Producer(
    var kaBI: BigInteger = BigInteger.ONE,
    var xBI: List<BigInteger> = emptyList(),
    var outBI: List<BigInteger> = emptyList(),
    var cBI: List<BigInteger> = emptyList()
) {
    constructor(queryStage1: QueryStage1) : this() {
        kaBI = BigInteger(queryStage1.ka)
        xBI = queryStage1.x.map{ BigInteger(it) }
        outBI = queryStage1.out.map{BigInteger(it)}
        cBI = queryStage1.c.map{BigInteger(it)}
    }
    fun getQueryStage1(): QueryStage1 {
        val ka = kaBI.toString(10)
        val x = xBI.map { it.toString(10) }
        val out = outBI.map { it.toString(10) }
        val c = cBI.map { it.toString(10) }
        return QueryStage1(ka, x, out, c)
    }
}
package com.example.secrettinder.data

class User(var name : String = "", var contact: String = "", var description: String = ""){
    constructor(map: Map<String, String>) : this() {
        name = map["name"]!!
        contact = map["contact"]!!
        description = map["description"]!!
    }
    init {
        if( name == "") {
            name = this.hashCode().toString()
        }
    }
}
package com.example.secrettinder.data

import java.math.BigInteger

data class QueryStage3(var mp : List<String> = emptyList())

class QueryStage3Producer(
    var mpBI: List<BigInteger> = emptyList(),
) {
    constructor(queryStage3: QueryStage3) : this() {
        mpBI = queryStage3.mp.map{ BigInteger(it) }
    }
    fun getQueryStage3(): QueryStage3 {
        val mp = mpBI.map{it.toString(10)}
        return QueryStage3(mp)
    }
}

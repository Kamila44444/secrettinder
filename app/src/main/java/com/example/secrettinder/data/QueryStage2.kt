package com.example.secrettinder.data

import java.math.BigInteger

data class QueryStage2(var v: String)


class QueryStage2Producer(
    var vBI: BigInteger = BigInteger.ONE,
) {
    constructor(queryStage2: QueryStage2) : this() {
        vBI = BigInteger(queryStage2.v)
    }
    fun getQueryStage2(): QueryStage2 {
        val v = vBI.toString(10)
        return QueryStage2(v)
    }
}

package com.example.secrettinder.data

import java.math.BigInteger

data class QueryStage4(var result: String)


class QueryStage4Producer(
    var resultBI: BigInteger = BigInteger.ONE,
) {
    constructor(queryStage4: QueryStage4) : this() {
        resultBI = BigInteger(queryStage4.result)
    }
    fun getQueryStage4(): QueryStage4 {
        val result = resultBI.toString(10)
        return QueryStage4(result)
    }
}
